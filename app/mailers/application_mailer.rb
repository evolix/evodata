class ApplicationMailer < ActionMailer::Base
  default from: "evodata@evolix.org"
  layout "mailer"
end
