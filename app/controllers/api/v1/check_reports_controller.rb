class Api::V1::CheckReportsController < Api::V1::BaseController
  # POST /check-reports or /check-reports.json
  def create
    @check_report = CheckReport.new(check_report_params)

    if @check_report.checks.empty?
      render json: {message: "At least 1 check must be present"}, status: :unprocessable_entity
    else
      checks_params = @check_report.checks.map { |check|
        {
          hostname: @check_report.hostname,
          date: @check_report.date,
          name: check.fetch("name", ""),
          description: check.fetch("description", "")
        }
      }
      checks = Check.create(checks_params)

      persisted = checks.count(&:persisted?)
      invalid = checks.count(&:invalid?)

      if checks.all?(&:persisted?)
        render json: {message: "#{persisted} checks created"}, status: :created
      else
        render json: {message: "error while creating checks : #{persisted} persisted, #{invalid} invalid"}, status: :unprocessable_entity
      end
    end
  end

  private

  # Only allow a list of trusted parameters through.
  def check_report_params
    params.require(:check_report).permit(:hostname, :date, {checks: [:name, :description]})
  end
end
