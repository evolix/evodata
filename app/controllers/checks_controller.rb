class ChecksController < AuthenticatedController
  before_action :set_check, only: %i[show edit update destroy]

  # GET /checks or /checks.json
  def index
    # @search = Check.ransack(params[:q])
    # @checks = @search.result
    @checks = Check.order(date: :desc, hostname: :asc, name: :asc).page params[:page]
  end

  # GET /checks/1
  def show
  end

  # GET /checks/new
  def new
    @check = Check.new
  end

  # GET /checks/1/edit
  def edit
  end

  # POST /checks
  def create
    @check = Check.new(check_params)

    if @check.save
      redirect_to check_url(@check), notice: "Check was successfully created."
    else
      render :new, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /checks/1
  def update
    if @check.update(check_params)
      redirect_to check_url(@check), notice: "Check was successfully updated."
    else
      render :edit, status: :unprocessable_entity
    end
  end

  # DELETE /checks/1
  def destroy
    @check.destroy

    redirect_to checks_url, notice: "Check was successfully destroyed."
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_check
    @check = Check.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def check_params
    params.require(:check).permit(:name, :description, :hostname)
  end
end
