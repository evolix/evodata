import { Controller } from "@hotwired/stimulus"

export default class extends Controller {
  static targets = [ "notificationWrapper" ]

  dismiss () {
    this.notificationWrapperTargets.forEach((element, index) => {
      element.classList.add("hidden")
    })
  }
}
