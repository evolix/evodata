import { Controller } from "@hotwired/stimulus"

export default class extends Controller {
  static targets = [ "userButton", "userMenu", "mobileButton", "mobileMenu", "mobileOpenButton", "mobileCloseButton" ]

  // connect () {
  //   console.log("main-nav connected")
  //   this.hide_user_menu()
  // }
  toggle_user_menu () {
    if (this.userButtonTarget.getAttribute('aria-expanded') == "false") {
      this.show_user_menu()
    } else {
      this.hide_user_menu()
    }
  }
  show_user_menu () {
      this.userButtonTargets.forEach((element, index) => {
        element.setAttribute('aria-expanded', 'true');
      })
      this.userMenuTargets.forEach((element, index) => {
        element.classList.remove("hidden")
      })
  }
  hide_user_menu () {
    this.userButtonTargets.forEach((element, index) => {
      element.setAttribute('aria-expanded', 'false');
    })
    this.userMenuTargets.forEach((element, index) => {
      element.classList.add("hidden")
    })
  }
  toggle_mobile_menu () {
    if (this.mobileButtonTarget.getAttribute('aria-expanded') == "false") {
      this.show_mobile_menu()
    } else {
      this.hide_mobile_menu()
    }
  }
  show_mobile_menu () {
      this.mobileButtonTargets.forEach((element, index) => {
        element.setAttribute('aria-expanded', 'true');
      })
      this.mobileMenuTargets.forEach((element, index) => {
        element.classList.remove("hidden")
      })
      this.mobileOpenButtonTargets.forEach((element, index) => {
        element.classList.add("hidden")
      })
      this.mobileCloseButtonTargets.forEach((element, index) => {
        element.classList.remove("hidden")
      })
  }
  hide_mobile_menu () {
    this.mobileButtonTargets.forEach((element, index) => {
      element.setAttribute('aria-expanded', 'false');
    })
    this.mobileMenuTargets.forEach((element, index) => {
      element.classList.add("hidden")
    })
    this.mobileCloseButtonTargets.forEach((element, index) => {
      element.classList.add("hidden")
    })
    this.mobileOpenButtonTargets.forEach((element, index) => {
      element.classList.remove("hidden")
    })
  }
}
