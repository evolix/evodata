class Check < ApplicationRecord
  validates_presence_of :name
  validates_presence_of :hostname
end
