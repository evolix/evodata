class CheckReport
  include ActiveModel::API
  include ActiveModel::Attributes

  attribute :hostname, :string
  attribute :date, :datetime, default: -> { Time.now }
  attribute :checks, array: true
end
