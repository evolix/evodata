class ApiKey < ApplicationRecord
  encrypts :token, deterministic: true

  belongs_to :bearer, polymorphic: true

  def self.authenticate_by_token!(token)
    find_by! token: token
  end

  def self.authenticate_by_token(token)
    authenticate_by_token! token
  rescue ActiveRecord::RecordNotFound
    nil
  end
end
