module ApplicationHelper
  def colored_pill_tag(content_or_options_with_block = nil, options = nil, &block)
    if block
      options = content_or_options_with_block if content_or_options_with_block.is_a?(Hash)
    end

    color_classes = case options.fetch(:color)
    when "red"
      "bg-red-100 text-red-800"
    when "green"
      "bg-green-100 text-green-800"
    when "orange"
      "bg-orange-100 text-orange-800"
    else
      "bg-gray-100 text-gray-800"
    end

    classes = "px-2 inline-flex text-xs leading-5 font-semibold rounded-full #{color_classes}"

    if block
      content_tag(:span, class: classes, &block)
    else
      content_tag(:span, content_or_options_with_block, class: classes)
    end
  end
end
