Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      defaults format: :json do
        get "/ping", to: "base#ping"
        resources :check_reports, path: "check-reports", only: [:create]
      end
    end
  end

  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html
  root "sessions#new"

  post "sign_up", to: "accounts#create"
  get "sign_up", to: "accounts#new"

  put "account", to: "accounts#update"
  get "account", to: "accounts#edit"
  delete "account", to: "accounts#destroy"

  resources :confirmations, only: [:create, :edit, :new], param: :confirmation_token

  post "login", to: "sessions#create"
  delete "logout", to: "sessions#destroy"
  get "login", to: "sessions#new"

  resources :passwords, only: [:create, :edit, :new, :update], param: :password_reset_token

  resources :checks
  resources :users
end
