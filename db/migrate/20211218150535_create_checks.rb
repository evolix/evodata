class CreateChecks < ActiveRecord::Migration[7.0]
  def change
    create_table :checks do |t|
      t.string :name
      t.text :description
      t.string :hostname

      t.timestamps
    end
    add_index :checks, :name
    add_index :checks, :hostname
  end
end
