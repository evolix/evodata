class AddDateToChecks < ActiveRecord::Migration[7.0]
  def change
    add_column :checks, :date, :datetime, null: false
  end
end
