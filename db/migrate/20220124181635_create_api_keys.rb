class CreateApiKeys < ActiveRecord::Migration[7.0]
  def up
    create_table :api_keys do |t|
      t.string :token
      t.references :bearer, polymorphic: true, null: false
    end

    add_index :api_keys, [:bearer_id, :bearer_type]
    add_index :api_keys, :token, unique: true
  end

  def down
    drop_table :api_keys
  end
end
